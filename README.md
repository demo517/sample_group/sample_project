# Pipeline Demo Repository

## Overview

This repo contains code for the AzureDevOps and Gitlab CI Pipelines comparison. Here you will find sample code for demonstrating how to convert an Azure DevOps Pipeline to a Gitlab pipeline that can be triggered manually.

## Folder Structure

```txt
|.gitlab-ci/
|__jobs/
|  |__eslint_template.yml
|  |__pylint_template.yml
|  |__pytest_template.yml
|__pipelines/
|  |__testing_pipeline.yml
|app/
|__javascript/
|__python/
|pipeline_demo_walkthrough/

```
