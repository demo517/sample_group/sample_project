from python.func import my_func


def test_func():
    actual = my_func()
    expected = "A python function"
    assert actual == expected
